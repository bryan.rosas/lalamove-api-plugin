<?php
/***
 * @package Lalamove API Pluginin
 */


if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . '/wp-admin/includes/class-wp-list-table.php' );
}

class WPListTable extends WP_List_Table {

	/**
	 * Prepares the list of items for displaying.
	 */
	public function prepare_items() {

		$order_by = isset( $_GET['orderby'] ) ? $_GET['orderby'] : '';
		$order = isset( $_GET['order'] ) ? $_GET['order'] : '';
		$search_term = isset( $_POST['s'] ) ? $_POST['s'] : '';

		$this->items = $this->wlt_list_table_data( $order_by, $order, $search_term );

		$wlt_columns = $this->get_columns();
		$wlt_hidden = $this->get_hidden_columns();
		$ldul_sortable = $this->get_sortable_columns();

		$this->_column_headers = [ $wlt_columns, $wlt_hidden, $ldul_sortable ];

	}

	/**
	 * Wp list table bulk actions 
	 */
	public function get_bulk_actions() {

		//  return array(

		// 	'wlt_delete'	=> __( 'Delete', "Lalamove API Plugin" ),
		// 	'wlt_edit'		=> __( 'Edit', "Lalamove API Plugin" )
		// );
	}

	/**
	 * WP list table row actions
	 */
	public function handle_row_actions( $item, $column_name, $primary ) {

		if( $primary !== $column_name ) {
			return '';
		}
		
		$action = [];
		$action['View'] = '<a class="wlt-edit" data-target="#myModal" data-toggle="modal"  href="#myModal" >'.__( 'View', "Lalamove API Plugin" ).'</a>';
		// $action['delete'] = '<a class="wlt-delete-post">'.__( 'Delete', "Lalamove API Plugin" ).'</a>';
		// $action['quick-edit'] = '<a>'.__( 'Update', "Lalamove API Plugin" ).'</a>';
		// $action['view'] = '<a>'.__( 'View', "Lalamove API Plugin" ).'</a>';
 
		return $this->row_actions( $action );
	}

	/**
	 * Display columns datas
	 */
	public function wlt_list_table_data( $order_by = '', $order = '', $search_term = '' ) {

		?><section style="margin: 30px 0 0 0; ">
			<h2><?php _e( 'Pending:', "Lalamove API Plugin" ); ?></h2>
			<?php

		//WP Query to find Customer
		$customer_orders = get_posts( array( 
			'post_type' => 'shop_order',
			'post_status'    => array_keys( wc_get_order_statuses() ) 
		) );
		
		$countries_obj = new WC_Countries();

		$data_array = [];

		$country_states_array = $countries_obj->get_states();

		if( $customer_orders ) {

			foreach( $customer_orders as $customer_order ) {

				$order_id = $customer_order->ID;

				$order = wc_get_order( $order_id );
				$order_data = $order->get_data();

				$currency_code = $order->get_currency();
				$currency_symbol = get_woocommerce_currency_symbol( $currency_code );


				$name = $order_data["shipping"]["first_name"]." ".$order_data["shipping"]["last_name"];
				$address = $order_data['shipping']['address_1'].", ". $order_data['shipping']['city'].", "
						.$country_states_array[$order_data['shipping']['country']][ $order_data['shipping']['state']];//.","
						// .$country_country_array[$order_data['shipping']['country']].","				
						//.$order_data['shipping']['postcode']; //remomve
				
				$date = get_the_date( 'l F j, Y', $customer_order->ID );
				$total = $order_data["total"];


				foreach( $order->get_items( 'shipping' ) as $item_id => $item ){
					$shipping_method_total  = $item->get_total();
				}
			
				// Conditional function based on the Order shipping method 
				if( $order->has_shipping_method("lalamove") ) { 

					$dataorder = array(	
							'name' 			=> $name,
							'address' 		=> $address,
							'date' 			=> $date, 
							'shipping_total' => $shipping_method_total,
							'currency' 		=>$currency_symbol,
							'total'			=> $total
					);
				
					//$result =  json_encode($dataorder) ;

					$data_array[] = [
						'wlt_id'				=> '<a data-order-id="'.$order_id.'" data-order-customer="'.htmlspecialchars( json_encode($dataorder) ).'" href="'.get_edit_post_link( $order_id ).'"> #'.$order_id.' - '.$name.'</a>',
						'wlt_address'			=> $address,
						'wlt_publish_data'		=> $date,
						'wlt_total'				=> $currency_symbol.$total,
					];

				}	
			}
		}

		?></section><?php
	    return $data_array;

	}

	/**
	 * Gets a list of all, hidden and sortable columns
	 */
	public function get_hidden_columns() {
		return [];
	}

	/**
	 * Gets a list of columns.
	 */
	public function get_columns() {	

		$columns = array(
			'cb'				=> '<input type="checkbox" class="wlt-selected" />',
			'wlt_id'			=> __( 'Order', 'Lalamove API Plugin' ),
			'wlt_address'			=> __( 'Address', 'Lalamove API Plugin' ),
			'wlt_publish_data'	=> __( 'Date', 'Lalamove API Plugin' ),
			'wlt_total'		=> __( 'Total', 'Lalamove API Plugin' ),
		);
		return $columns;
	}

	/**
	 * Return column value
	 */
	public function column_default( $item, $column_name ) {

		switch ($column_name) {
			case 'wlt_id':
			case 'wlt_address':
			case 'wlt_publish_data':
			case 'wlt_total':
			
			return $item[$column_name];
			default:
			return 'no list found';
		}
	}

	/**
	 * Rows check box
	 */
	public function column_cb( $items ) {

		$top_checkbox = '<input type="checkbox" class="wlt-selected" />';
		return $top_checkbox; 
	}
}


$object = new WPListTable();
$object->prepare_items();
$object->display();
