<div class="wrap">
    <h1>Settings</h1>
    <br>
    <?php settings_errors(); ?>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#cost">Lalamove Cost</a></li>
	    <li><a data-toggle="tab" href="#lalakey">Lalamove API Key</a></li>
	    <li><a data-toggle="tab" href="#googlekey">Google API Key</a></li>
  	</ul>
  	<div class="tab-content">
      <div id="cost" class="tab-pane fade in active">
            <form method="post" action="options.php">
                <?php
                    settings_fields( 'shipping_options_group' );
                    do_settings_sections( 'weight_cost' );
                    submit_button();
                ?>
            </form>
	    </div>
	    <div id="lalakey" class="tab-pane fade">
            <form method="post" action="options.php">
                <?php
                    settings_fields( 'Lalamove_options_group' );
                    do_settings_sections( 'lalamove_keys' );
                    submit_button();
                ?>
            </form>
	    </div>
    	<div id="googlekey" class="tab-pane fade">
            <form method="post" action="options.php">
                <?php
                    settings_fields( 'google_options_group' );
                    do_settings_sections( 'google_keys' );
                    submit_button();
                ?>
            </form>
    	</div>
    
  	</div>
</div>
