jQuery(document).ready(function() {

	$('ul.nav-tabs > li').click(function(){
		var data = $(this).attr("href");

	});
   
	$( '.wlt-edit' ).on( 'click', function() {

		let self = $( this );
		let parent = self.parents( 'tr' );
		let OrderID = parent.find( '.wlt_id a' ).attr( 'data-order-id' );
		var customer = parent.find( '.wlt_id a' ).attr( 'data-order-customer' );
		var Data = JSON.parse(customer)
	
		var Name = Data.name ;
		var LAddress = Data.address;
		var subbtotal =  parseFloat(Data.total - Data.shipping_total);

		$('.modal-title').text("Order no: "+OrderID);
		$('.customerid').val(OrderID);
		$('.L_Name').text(Name);
		$('.L_Address').text(LAddress);
		$('.L_Date').text(Data.date);
		$('.L_Subtotal').html(Data.currency+""+subbtotal.toFixed(2));
		$('.L_Shippinng').html(Data.currency+""+Data.shipping_total);
		$('.L_Total').html(Data.currency+""+Data.total);
	
	} );

	$( '.order-update' ).on( 'click', function() {

		let orderID = $( '.customerid' ).val();
		let postContent = "sample";
  
		let ajaxurl = wlt.ajax_url;
		let data = {
		  'action'      : 'wlt_edit_update_post',
		  'post_id'     : orderID,
		  'post_content': postContent
		}
  
		jQuery.post( ajaxurl, data, function( responce ) {
			location.reload( true );
		} );

		//console.log(orderID);
  
	} );
	
});  

