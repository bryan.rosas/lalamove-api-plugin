<?php
/**
* Plugin Name: Lalamove API
* Plugin URI: https://www.mdscsi.com/
* Description: LALAMOVE API for EWS.
* Version: 1.0
* Author:Technology Engineer - MDSCSI
* Author URI: http://mdscsi.com/
**/

// If this file is called firectly, abort!!!
defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

//Require once the Autoload Composer
if( file_exists( dirname( __FILE__ ). '/vendor/autoload.php') ){
    require_once dirname( __FILE__ ). '/vendor/autoload.php';
}

function activate_lalamove_plugin() {
    Lalamove\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_lalamove_plugin' );


function deactivate_lalamove_plugin() {
    Lalamove\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_lalamove_plugin' );

/**
 * Initialize All the Core Classes of this Plugin
 * */
if( class_exists( 'Lalamove\\Init')){
    Lalamove\Init::register_services();
}
