<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd78fbf18234680cb9576ce969f7b3b24
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'Lalamove\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Lalamove\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd78fbf18234680cb9576ce969f7b3b24::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd78fbf18234680cb9576ce969f7b3b24::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitd78fbf18234680cb9576ce969f7b3b24::$classMap;

        }, null, ClassLoader::class);
    }
}
