<?php
/**
* @package LalamovePlugin 
**/

namespace Lalamove\Base;

use \Lalamove\Base\LalaController;


class SettingsLinks extends LalaController
{
    public function register(){
        add_filter( "plugin_action_links_$this->plugin_bname", array( $this, 'settings_links') );
    }

    public function settings_links( $links ) {

        $settings_links = '<a href="admin.php?page=lalamove_plugin">Settings</a>';
        array_push( $links , $settings_links );
        return $links;
    }

}