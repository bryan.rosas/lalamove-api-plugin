<?php
/**
* @package LalamovePlugin 
**/

namespace Lalamove\Base;

class Activate {

    public static function activate(){
        flush_rewrite_rules();     
        
        if( get_option( 'lalamove_api_key' ) && get_option( 'google_api_key' )){
            return;
        }

        $default = array();

        update_option ('lalamove_api_key', $default);
        update_option ('google_api_key', $default);
    }

}