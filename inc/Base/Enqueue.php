<?php
/**
* @package LalamovePlugin 
**/

namespace Lalamove\Base;

use \Lalamove\Base\LalaController;

class Enqueue extends LalaController
{

    function register(){
        add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );        
    }

    function enqueue(){
        wp_enqueue_style( 'myplugin-style', $this->plugin_url.'assets/css/mystyle.css' );
        wp_enqueue_script( 'myplugin-js', $this->plugin_url.'assets/js/main.js' );

        wp_enqueue_script( 'Jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" );
        wp_enqueue_script( 'BT-JS', "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" );

    }
}

