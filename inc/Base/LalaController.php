<?php
/***
 * @package Lalamove Plugin
 */

 namespace Lalamove\Base;

 class LalaController 
 {
    public $plugin_path;

    public $plugin_url;

    public $plugin_bname;

    public $fields = array();

    public $groupsection = array();
    
    //Customer Data
    public $customerdetail = array();
    public $shippingdetail = array();
    
    public function __construct() {
        $this->plugin_path = plugin_dir_path( dirname( __FILE__, 2 ) );
        $this->plugin_url = plugin_dir_url( dirname( __FILE__, 2 ) );
        $this->plugin_bname = plugin_basename( dirname( __FILE__, 3 ) .'/lalamove-plugin.php');

        $this->fields = array(
            'host_name' => 'Host Name',
            'api_key'   => 'API Keys',
            'secret_key'    => 'Secret Keys',
            'country'   => 'Country',
            'google_keys'   => 'Google API',
            'modal' => '',
            'cost_max' => 'Max Weight Cost (100kg)',
            'cost_median' => 'Median Weight Cost (50kg)',
            'cost_min' => 'Minimum Weight Cost ( below 10kg)',
            //'cost_basic' => 'Basic Cost',
        );
        
        $this->cost = array(
            'max_cost' => 'Max Weight',
            'min_cost'   => 'Min Weight',
            'default'    => 'Default'      
        );

        $this->groupsection = array(
            'Lalamove_options_group' => 'lalamove_api_key',
            'google_options_group'   => 'google_api_key'
        );

        $this->customerdetail = array(
            'L_Name'    => 'Customer Name: ',
            'L_Address' => 'Address: ',
            'L_Date'    => 'Date: '
        );

        $this->shippingdetail = array(
            'L_Subtotal'    => 'Subtotal',
            'L_Shippinng' => 'Shipping',
            'L_Total'   => 'Total'
        );

    }
 }