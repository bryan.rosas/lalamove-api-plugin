<?php
/**
* @package LalamovePlugin 
**/

namespace Lalamove\Pages;

use \Lalamove\Base\LalaController;
use \Lalamove\API\SettingsAPI;

use \Lalamove\API\CallBacks\AdminCallBacks;
use \Lalamove\API\CallBacks\FieldsCallBacks;

use \Lalamove\API\WPListTable;

class Admin extends LalaController
{

    public $settings;

    public $callbacks;
    public $callbacks_flds;

    public $pages = array();
    public $subpages = array();

    public $wp_list;

    public function register(){

        $this->settings = new SettingsAPI();

        $this->callbacks = new AdminCallBacks();
        $this->callbacks_flds = new FieldsCallBacks();

        $this->setPages();
 
        $this->setSubPages();

        $this->setSettings();
        $this->setSections();
        $this->setFields();                

        $this->settings->addPages( $this->pages )->SubPage('Dashboard')->addSubPages( $this->subpages )->register();
        
    }

    public function setPages()
    {
        $this->pages = array(
            array(
                'page_title' => 'Lalamove Plugin',
                'menu_title' => 'Lalamove', 
                'capability' => 'manage_options', 
                'menu_slug'  => 'lalamove_plugin', 
                'callback'   =>  array( $this->callbacks, 'adminDashboard' ), //call Back the Admin Templates
                'icon_url'   => 'dashicons-car',
                'position'   => 200
            )
        );
    }
    
    public function setSubPages()
    { 
        $this->subpages = array(
            array(
                'parent_slug' => 'lalamove_plugin',
                'page_title' => 'Order',
                'menu_title' => 'Order', 
                'capability' => 'manage_options', 
                'menu_slug'  => 'lalamove_order',
                'callback'   => array( $this->callbacks, 'adminOrder' )
            ),
            array(
                'parent_slug' => 'lalamove_plugin',
                'page_title' => 'Settings API Keys',
                'menu_title' => 'Settings', 
                'capability' => 'manage_options', 
                'menu_slug'  => 'lalamove_keys',
                'callback'   => array( $this->callbacks, 'adminSetting' )
            )
        );    
    }

    public function setSettings()
    {
       
        $args = array();

        foreach( $this->groupsection as $key => $value){


            $args[]= array(
                        'option_group'  => $key,
                        'option_name'   => $value,
                        'callback'  => array( $this->callbacks_flds, 'LalamoveOptionsGroup' )
            );
        }

        $this->settings->setSettings( $args );

    }

    public function setSections()
    {
        $args = array (
            array(
                'id'  => 'lalamove_api_settings',
                'title'   => '',
                'callback'  => array( $this->callbacks_flds, 'LalamoveSettingsSection' ),
                'page' => 'lalamove_keys'
            ),
            array(
                'id'  => 'goole_api_settings',
                'title'   => '',
                'callback'  => array( $this->callbacks_flds, 'GoogleSettingsSection' ),
                'page' => 'google_keys'
            ),
            array(
                'id'  => 'lalamove_weight_cost',
                'title'   => '',
                'callback'  => array( $this->callbacks_flds, 'LalamovecostSettingsSection' ),
                'page' => 'weight_cost'
            ),
            array(
                'id'  => 'lalamove_admin_index',
                'title'   => '',
                'callback'  => array( $this->callbacks, 'LalamoveOrderSection' ),
                'page' => 'lalamove_order'
            )
        );
        $this->settings->setSections( $args );
    }

    public function setFields()
    {
        $args = array();

        foreach( $this->fields as $key => $value){

            if($key == 'google_keys'){
                $page = 'google_keys';
                $section = 'goole_api_settings';
                $optname = 'google_api_key';
                $callkeys = array( $this->callbacks_flds, 'TextBoxFields' );
            
            }else{
                $page = 'lalamove_keys';
                $section = 'lalamove_api_settings';
                $optname = 'lalamove_api_key';
                $callkeys = array( $this->callbacks_flds, 'TextBoxFields' );
            }

            if($key == 'modal'){
                $page = 'lalamove_order';
                $section = 'lalamove_admin_index';
                $optname = 'modal';
                $callkeys = array( $this->callbacks, 'OrderViewTable' );

            }

            if (strpos($value, 'Cost') !== false) {
                $page = 'weight_cost';
                $section = 'lalamove_weight_cost';
                $optname = 'weight_cost';
                $callkeys = array( $this->callbacks_flds, 'TextBoxFields' );
            }


            $args[]=  array(
                        'id'  => $key,
                        'title'   => $value,
                        'callback'  => $callkeys,
                        'page' => $page,
                        'section' => $section,
                        'args'  => array(
                               'label_for' => $key,
                               'class' => 'regular-text',
                               'placeholder' => $value,
                               'option_name' => $optname
                            )      
                     ) ;
        }
        
        $this->settings->setFields( $args );  
    }


}