<?php
/***
 * @package Lalamove Plugin
 */

 namespace Lalamove\API\CallBacks;

 use Lalamove\Base\LalaController;


 class AdminCallBacks extends LalaController
 {
     public function adminDashboard()
     {
        return require_once ( "$this->plugin_path/templates/admin-pages.php" );
     }
      
     public function adminOrder()
     {
        return require_once ( "$this->plugin_path/templates/order-pages.php" );
     } 

     public function adminSetting()
     {
        return require_once ( "$this->plugin_path/templates/settings-pages.php" );
     }

     public function LalamoveOrderSection()
     {
       return require_once ( "$this->plugin_path/templates/WPListTable.php" );
     }

     public function OrderViewTable()
     {
       ?>  
       <!-- Modal -->
       <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
             <!-- Modal content-->
             <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Order Number</h4>
             </div>
             <div class="modal-body">      
                <input type="hidden" class="customerid" id="customerid" value="">            
                <div class="contianer">
                   <div class="row">
                     <?php foreach( $this->customerdetail as $key => $value){ ?>
                        <div class="col-12">
                           <div class="col-6">
                              <p class="Lbel"><?php echo $value; ?></p> 
                           </div>
                           <div class="col-6">
                              <p class="<?php echo $key; ?>"></p> 
                           </div>
                        </div>
                     <?php } ?>                  
                  </div>
                   <hr>
                   <div class="row">
                     <?php foreach( $this->shippingdetail as $key => $value){ ?>
                        <div class=" col-12">
                            <div class="col-3">
                               <p class="f-right Lbel"><?php echo $value; ?></p>
                            </div>
                            <div class="f-right col-3">
                               <p class="f-right <?php echo $key; ?>"></p>
                            </div>
                         </div> 
                     <?php } ?> 
                   </div>
                </div>           
             </div>
             <div class="modal-footer">
                      <input class="order-update button-primary" type="submit" value="close">
             </div>
          </div>    
       </div>
      <?php
     }

 }

