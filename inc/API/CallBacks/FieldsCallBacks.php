<?php
/***
 * @package Lalamove Plugin
 */

 namespace Lalamove\API\CallBacks;

 use Lalamove\Base\LalaController;


 class FieldsCallBacks extends LalaController
 {
   
    public function LalamoveOptionsGroup( $input )
    {
      return $input;
    }
    
    public function LalamoveSettingsSection()
    {
      echo "<br>";
      echo "Get API Keys on <a href='https://partnerportal.lalamove.com/' target='_blank'>Partner Portal Lalamove</a>";
    }

    public function GoogleSettingsSection()
    {
      echo "<br>";
      echo "Get API Keys on <a href='https://console.cloud.google.com/' target='_blank'>Cloud Google Console</a>";
    }

    public function LalamovecostSettingsSection()
    {
      echo "<br>";
      echo "Weight Cost";
    }

    public function TextBoxFields( $args )
    { 
       $name = $args['label_for'];
       $classes = $args['class'];
       $placehold = $args['placeholder'];
       $option_name = $args['option_name'];
       $value = esc_attr(  get_option( $option_name )[$name] );

       echo '<input type="text" class="'. $classes.'" name="'.$option_name.'['.$name.']'.'" value="'.$value.'" placeholder="'.$placehold.'">';
    }

    public function CheckBoxFields( $args ){

      $name = $args['label_for'];
      $classes = $args['class'];
      $placehold = $args['placeholder'];
      $option_name = $args['option_name'];
      $value = esc_attr(  get_option( $option_name )[$name] );

    }


 }
